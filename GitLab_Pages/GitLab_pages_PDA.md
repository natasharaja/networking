---
title: 'GitLab Pages'
subtitle: 'GitLab'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'GitLab Pages'
---

# Audience

Students and lectureres at UCL.

---

# Purpose

The purpose of this document is to provide students and Lecturers an entry level guide to GitLab pages and for me Per dahlstrøm to take notes on what I am doing.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# jekyll  

In order to make the GitLab md documents into a Web Site this seems to be a good starting point.

https://github.com/pmarsceill/just-the-docs



# iot-with-particle

This is Nikolaj and Mortens take on the 2 semester iot-with-particle project spring 2020:

https://gitlab.com/EAL-ITT/iot-with-particle/-/tree/master

