# Test program for the Device class
from jnpr.junos import Device
import base64
# Device.auto_probe = 3
# Create device object
myDev = Device(host='192.168.157.10', user='root', password='Rootpass')
# Test for 3 seconds if device is reachable
myDev.auto_probe = 3
# Connect to device
myDev.open()
print myDev.cli("show interfaces terse", warning=False) # Disable warnings
myDev.close()

