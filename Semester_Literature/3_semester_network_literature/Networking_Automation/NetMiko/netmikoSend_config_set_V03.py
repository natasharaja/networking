#!/usr/bin/python3
# The program sets ge-0/0/4 and ge-0/0/6
# Please do not configure ge-0/0/5 as it is default gateway
# It is a "primitive" program to show the very basics of configuring.
# "send_config_set" per default automatically enters/exits configuration mode.
# This is changed with the kwarg: exit_config_mode=False to allow the commit
# By Per Dahlstroem 2019

import netmiko

mySetConfigs =    ['set interfaces ge-0/0/4 unit 0 family ' + \
                   'inet address 192.168.4.1/24',
                   'set interfaces ge-0/0/6 unit 0 family ' + \
                   'inet address 192.168.6.1/24']
myDeleteConfigs = ['delete interfaces ge-0/0/4',
                   'delete interfaces ge-0/0/6']
try:
        myConnection = netmiko.ConnectHandler(ip='192.168.2.1',
                                              device_type='juniper',
                                              username='root',
                                              password='Rootpass')
        myConfigurationCommands = mySetConfigs
#       myConfigurationCommands = myDeleteConfigs
        print('Setting interface(s)')
        print(myConnection.send_config_set(myConfigurationCommands,
                                              exit_config_mode=False))
        print(myConnection.commit(and_quit=True)) # Exit configuration mode
        print(myConnection.send_command('show route terse'))
        print('Closing connection')
        myConnection.disconnect()
        print('Connection closed')
except Exception as e: # Catch and print any exception
        print(e)
