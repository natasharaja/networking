#!/usr/bin/python2
# From: https://www.youtube.com/watch?v=b0wk12EwnPQ
# Very short program to demonstrate netmiko send_command

import netmiko

myConnection = netmiko.ConnectHandler(ip='192.168.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='Rootpass')
print myConnection.send_command('show route terse')
myConnection.disconnect()