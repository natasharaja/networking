---
title: 'Network SSH'
subtitle: 'Linux and Windows'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Network SSH'
---

By Per Dahlstroem pda@ucl.dk

# Audience

The main audience is students at the two year IT Technology education at UCL in Odense Denmark ucl.dk/international

---

# Purpose

The purpose of this document is to provide students with some basic insight intu how to backup and restore of a virtual network system. Mainly on VMWW.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This generic network diagram serves as the foundation for describing and demonstration.

The network was build in VMware Workstation.


![Semantic description of image](SSH_Generic_Network_PDA_V01.JPG "Generic network diagram")  

---

# man

In general the man progaram serves as the portal to access the manuals for Linux programs.

* `man ssh`

Display the manual for any program by the man command. E.g. here the manual for the ssh client program.

* Type `q` to exit the manual.

---

In workstation select right click the machine to share or backup and selcet manage->share:

![Semantic description of image](images/Share_virtual_machine.JPG "")

Location of Virtual Machine files on the host computer:

Edit->Preferences

![Semantic description of image](images/Location_of_virtual_machine.JPG "")

Going to this location Virtual machine folders can be copied to another media or a file server.

Note that the file content probably is many Gb!

On another VMWW host machine the VM can be copied into that VMWWs VM fiolder.

* Run File->Scan for Virtual Machines
* Take owner ship when asked for this.
* Select Copied VM when asjked about it.

Now the VM is on another VMWW.
